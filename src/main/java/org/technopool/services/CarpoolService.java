package org.technopool.services;

import java.util.ArrayList;
import java.util.List;
import org.technopool.model.TravelOffer;
import org.technopool.model.TravelRequest;

/**
 * Exposes carpool travel requests and offers.
 * 
 * @author iwan
 */
public class CarpoolService {
    private final List<TravelRequest> travelRequests;
    private final List<TravelOffer> travelOffers;
    
    public CarpoolService() {
        travelRequests = new ArrayList<>();
        travelOffers = new ArrayList<>();
    }
    
        // ************************ REQUESTS *************************

    public List<TravelRequest> findAllRequests() {
        return travelRequests;
    }
    
    public boolean insertRequest(TravelRequest newRequest) {
        // Only perform the insert if the ID of the new request doesn't 
        // exist yet.
        if (!travelRequests.stream()
                            .anyMatch(r -> r.getId().equals(newRequest.getId()))) {
            travelRequests.add(newRequest);
            return true; 
        }
        
        return false;
    }
    
    public TravelRequest findRequest(final String id) {
        return travelRequests.stream()
                             .filter(r -> r.getId().equals(id))
                             .findFirst()
                             .orElse(null);
    }
    
    public TravelRequest updateRequest(TravelRequest updatedRequest) {
        final TravelRequest existing = findRequest(updatedRequest.getId());
        if (existing != null) {
            existing.update(updatedRequest);
            return updatedRequest;
        }
        
        return existing;
    }
    
    public boolean deleteRequest(final String id) {
        return travelRequests.removeIf(r -> r.getId().equals(id));
    }
    
    public boolean deleteAllRequests() {
        if (travelRequests.size() > 0) {
            travelRequests.clear();
            return true;
        } else {
            return false;
        }
    }
    
    public boolean isValidRequest(TravelRequest request) {
        return true;
    }
    
        // ************************ OFFERS *************************
    
    public List<TravelOffer> findAllOffers() {
        return travelOffers;
    }
    
    public boolean insertOffer(TravelOffer newOffer) {
        // Only perform the insert if the ID of the new offer doesn't 
        // exist yet.
        if (!travelOffers.stream()
                            .anyMatch(r -> r.getId().equals(newOffer.getId()))) {
            travelOffers.add(newOffer);
            return true; 
        }
        
        return false;
    }
    
    public TravelOffer findOffer(final String id) {
        return travelOffers.stream()
                             .filter(r -> r.getId().equals(id))
                             .findFirst()
                             .orElse(null);
    }
    
    public TravelOffer updateOffer(TravelOffer updatedOffer) {
        final TravelOffer existing = findOffer(updatedOffer.getId());
        if (existing != null) {
            existing.update(updatedOffer);
        }
        
        return existing;
    }
    
    public boolean deleteOffer(final String id) {
        return travelOffers.removeIf(r -> r.getId().equals(id));
    }

    public boolean deleteAllOffers() {
        if (travelOffers.size() > 0) {
            travelOffers.clear();
            return true;
        } else {
            return false;
        }
    }

    public boolean isValidOffer(TravelOffer travelOffer) {
        if (travelOffer.getSubscribedTravellers().size() > 
            travelOffer.getNrAvailableSeats()) {
            return false;
        } else {
            return true;
        }
    }
    
}
