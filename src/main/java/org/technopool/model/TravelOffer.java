package org.technopool.model;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Represents a carpool travel offer.
 * 
 * @author iwan
 */
public class TravelOffer {
    private String id;
    private String providerName;
    private String providerEmail;
    private String destination;
    private int nrAvailableSeats;
    private LocalDateTime leaveTime;
   
    private List<Traveller> subscribedTravellers;

    public TravelOffer() {
        this.subscribedTravellers = new ArrayList<>();
    }

    public TravelOffer(String id, String providerName, String providerEmail, 
            String destination, int nrAvailableSeats, LocalDateTime leaveTime, 
            List<Traveller> subscribedTravellers) {
        this.id = id;
        this.providerName = providerName;
        this.providerEmail = providerEmail;
        this.destination = destination;
        this.nrAvailableSeats = nrAvailableSeats;
        this.leaveTime = leaveTime;
        this.subscribedTravellers = subscribedTravellers;
    }
    
    public String getId() {
        return this.id;
    }

    public String getProviderName() {
        return this.providerName;
    }

    public String getProviderEmail() {
        return this.providerEmail;
    }

    public String getDestination() {
        return this.destination;
    }

    public int getNrAvailableSeats() {
        return this.nrAvailableSeats;
    }

    public LocalDateTime getLeaveTime() {
        return this.leaveTime;
    }

    public List<Traveller> getSubscribedTravellers() {
        return this.subscribedTravellers;
    }

    public void update(TravelOffer updatedOffer) {
        this.providerName = updatedOffer.providerName;
        this.providerEmail = updatedOffer.providerEmail;
        this.destination = updatedOffer.destination;
        this.nrAvailableSeats = updatedOffer.nrAvailableSeats;
        this.leaveTime = updatedOffer.leaveTime;
        this.subscribedTravellers = updatedOffer.subscribedTravellers;
    }
    
}
