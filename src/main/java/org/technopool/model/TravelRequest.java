package org.technopool.model;

import java.time.LocalDateTime;

/**
 * Represents a carpool travel request.
 * 
 * @author iwan
 */
public class TravelRequest {
    private String id;
    private String requesterName;
    private String requesterEmail;
    private String requesterDestination;
    private LocalDateTime desiredLeaveTimeStart;
    private LocalDateTime desiredLeaveTimeEnd;

    public TravelRequest() {
    }

    public TravelRequest(String id, String requesterName, String requesterEmail, 
            String requesterDestination, LocalDateTime desiredLeaveTimeStart, 
            LocalDateTime desiredLeaveTimeEnd) {
        this.id = id;
        this.requesterName = requesterName;
        this.requesterEmail = requesterEmail;
        this.requesterDestination = requesterDestination;
        this.desiredLeaveTimeStart = desiredLeaveTimeStart;
        this.desiredLeaveTimeEnd = desiredLeaveTimeEnd;
    }
    
    public String getId() {
        return this.id;
    }

    public String getRequesterName() {
        return this.requesterName;
    }

    public String getRequesterEmail() {
        return this.requesterEmail;
    }

    public String getRequesterDestination() {
        return this.requesterDestination;
    }

    public LocalDateTime getDesiredLeaveTimeStart() {
        return this.desiredLeaveTimeStart;
    }

    public void setDesiredLeaveTimeEnd(LocalDateTime desiredLeaveTimeEnd) {
        this.desiredLeaveTimeEnd = desiredLeaveTimeEnd;
    }
    
    public void update(TravelRequest other) {
        this.requesterName = other.requesterName;
        this.requesterEmail = other.requesterEmail;
        this.requesterDestination = other.requesterDestination;
        this.desiredLeaveTimeStart = other.desiredLeaveTimeStart;
        this.desiredLeaveTimeEnd = other.desiredLeaveTimeEnd;
    }
}
