package org.technopool.model;

/**
 * Represents a carpool traveller.
 * 
 * @author iwan
 */
public class Traveller {
    private String name;
    private String email;

    public Traveller(String travellerName, String travellerEmail) {
        this.name = travellerName;
        this.email = travellerEmail;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
