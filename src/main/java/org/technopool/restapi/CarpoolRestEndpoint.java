package org.technopool.restapi;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.http.HttpServletResponse;
import org.technopool.model.TravelOffer;
import org.technopool.model.TravelRequest;
import org.technopool.services.CarpoolService;
import spark.Spark;

/**
 * REST endpoint for carpool travel requests and offers.
 * 
 * @author iwan
 */
public class CarpoolRestEndpoint {
    private static final String API_CONTEXT = "/api/v1";
    
    private final CarpoolService carpoolService;
    private final Gson gson;
    
    public CarpoolRestEndpoint(CarpoolService carpoolService) {
        this.carpoolService = carpoolService;
        
        // Register a custom converter with GSON in order to encode and decode
        // Java 8 LocalDateTime objects.
        // ILI 17-apr-2016: It turns out that this custom converter can't handle times that end with 'Z' at 
        // the end. Because of this, the GsonUtcDateAdapter class was created, see Javadoc there.
        //this.gson = Converters.registerZonedDateTime(new GsonBuilder()).create();
        
        this.gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new GsonUtcDateAdapter()).create();

        setupCors();
        setupTravelRequests();
        setupTravelOffers();
    }

    /**
     * Set up CORS (Cross-origin resource sharing). See also 
     * http://www.mastertheboss.com/cool-stuff/create-a-rest-services-layer-with-spark
     */
    private void setupCors() {
        Spark.options("/*", (request,response)-> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if(accessControlRequestMethod != null){
            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        Spark.before((request,response)->{
            response.header("Access-Control-Allow-Origin", "*");
        });    
    }
    
    private void setupTravelRequests() {
        Spark.post(API_CONTEXT + "/requests", 
                   "application/json", 
                   (request, response) -> {
            TravelRequest travelReq = gson.fromJson(request.body(), 
                                                    TravelRequest.class);
            
            if (!carpoolService.insertRequest(travelReq)) {
                response.status(HttpServletResponse.SC_CONFLICT);
            } else {
                response.status(HttpServletResponse.SC_CREATED);
            }
            
            // Return a link to the newly created resource
            return API_CONTEXT + "/requests/" + travelReq.getId();
        }, gson::toJson);
        
        Spark.get(API_CONTEXT + "/requests/:id", 
                  "application/json", 
                  (request, response) -> {
            final TravelRequest travelReq = 
                    carpoolService.findRequest(request.params(":id"));
            
            if (travelReq == null) {
                response.status(HttpServletResponse.SC_NOT_FOUND);
            } else {
                response.status(HttpServletResponse.SC_OK);
            }
            
            return travelReq;
        }, gson::toJson);
        
        Spark.get(API_CONTEXT + "/requests", 
                  "application/json", 
                  (request, response) -> {
            return carpoolService.findAllRequests();
        }, gson::toJson);
       
        Spark.put(API_CONTEXT + "/requests/:id", 
                  "application/json", 
                  (request, response) -> {
            TravelRequest travelReq = gson.fromJson(request.body(), 
                                                    TravelRequest.class);
            TravelRequest resultReq = carpoolService.updateRequest(travelReq);
            if (resultReq == null) {
                response.status(HttpServletResponse.SC_NOT_FOUND);
            } else {
                response.status(HttpServletResponse.SC_OK);
            }
            
            return resultReq;
        }, gson::toJson);
        
        Spark.delete(API_CONTEXT + "/requests/:id", 
                  "application/json", 
                  (request, response) -> {
            final String id = request.params(":id");
            boolean result = carpoolService.deleteRequest(id);
            if (!result) {
                response.status(HttpServletResponse.SC_NOT_FOUND);
            } else {
                response.status(HttpServletResponse.SC_OK);
            }
            return "";
        }, gson::toJson);
        
        Spark.delete(API_CONTEXT + "/requests", 
                  "application/json", 
                  (request, response) -> {
            boolean result = carpoolService.deleteAllRequests();
            if (!result) {
                response.status(HttpServletResponse.SC_NO_CONTENT);
            } else {
                response.status(HttpServletResponse.SC_OK);
            }
            return "";
        }, gson::toJson);
    }

    private void setupTravelOffers() {
        Spark.post(API_CONTEXT + "/offers",
                "application/json",
                (request, response) -> {
                    TravelOffer travelOffer = gson.fromJson(request.body(),
                            TravelOffer.class);
                    
                    if (!carpoolService.isValidOffer(travelOffer)) {
                        response.status(422); // Unprocessable Entity
                    };
                    
                    if (!carpoolService.insertOffer(travelOffer)) {
                        response.status(HttpServletResponse.SC_CONFLICT);
                    } else {
                        response.status(HttpServletResponse.SC_CREATED);
                    }
                    
                    // Return a link to the newly created resource
                    return API_CONTEXT + "/offers/" + travelOffer.getId();
                }, gson::toJson);
        
        Spark.get(API_CONTEXT + "/offers/:id",
                "application/json",
                (request, response) -> {
                    final TravelOffer travelOffer =
                            carpoolService.findOffer(request.params(":id"));
                    
                    if (travelOffer == null) {
                        response.status(HttpServletResponse.SC_NOT_FOUND);
                    } else {
                        response.status(HttpServletResponse.SC_OK);
                    }
                    
                    return travelOffer;
                }, gson::toJson);
        
        Spark.get(API_CONTEXT + "/offers",
                "application/json",
                (request, response) -> {
                    return carpoolService.findAllOffers();
                }, gson::toJson);
        
        Spark.put(API_CONTEXT + "/offers/:id",
                "application/json",
                (request, response) -> {
                    TravelOffer travelOffer = gson.fromJson(request.body(),
                            TravelOffer.class);
                    if (!carpoolService.isValidOffer(travelOffer)) {
                        response.status(422); // Unprocessable Entity
                    };
                    
                    TravelOffer resultOffer = carpoolService.updateOffer(travelOffer);
                    if (resultOffer == null) {
                        response.status(HttpServletResponse.SC_NOT_FOUND);
                    } else {
                        response.status(HttpServletResponse.SC_OK);
                    }
                    
                    return resultOffer;
                }, gson::toJson);
        
        Spark.delete(API_CONTEXT + "/offers/:id",
                "application/json",
                (request, response) -> {
                    boolean result = carpoolService.deleteOffer(request.params(":id"));
                    if (!result) {
                        response.status(HttpServletResponse.SC_NOT_FOUND);
                    } else {
                        response.status(HttpServletResponse.SC_OK);
                    }
                    return "";
                }, gson::toJson);
        
        Spark.delete(API_CONTEXT + "/offers",
                "application/json",
                (request, response) -> {
                    boolean result = carpoolService.deleteAllOffers();
                    if (!result) {
                        response.status(HttpServletResponse.SC_NO_CONTENT);
                    } else {
                        response.status(HttpServletResponse.SC_OK);
                    }
                    return "";
                }, gson::toJson);
    }

}
