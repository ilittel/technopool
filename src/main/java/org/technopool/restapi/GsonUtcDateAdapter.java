package org.technopool.restapi;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Specialized Gson adapter class that can handle times with 'Z' at the end.
 * 
 * See also http://stackoverflow.com/questions/22310143/java-8-localdatetime-deserialized-using-gson
 */
class GsonUtcDateAdapter implements JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {

    public GsonUtcDateAdapter() {
    }

    @Override
    public JsonElement serialize(LocalDateTime t, Type type, JsonSerializationContext jsc) {
        final String dateTimeString = t.atOffset(ZoneOffset.UTC).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        
        return new JsonPrimitive(dateTimeString);
    }

    @Override
    public LocalDateTime deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        final String dateTimeString = json.getAsJsonPrimitive().getAsString();
        
        if (dateTimeString.endsWith("Z")) {
            return OffsetDateTime.parse(dateTimeString).toLocalDateTime();
        }
        
        return LocalDateTime.parse(dateTimeString);
    }
    
}
