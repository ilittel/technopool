package org.technopool;

import java.time.LocalDateTime;
import java.util.Arrays;
import org.technopool.model.TravelOffer;
import org.technopool.model.TravelRequest;
import org.technopool.model.Traveller;
import org.technopool.restapi.CarpoolRestEndpoint;
import org.technopool.services.CarpoolService;
import spark.Spark;

/**
 * Bootstrap class for Technopool Spark-based application.
 */
public class Bootstrap 
{
    public static void main( String[] args ) {
        final String ipAddress;
        if (args.length >= 2) {
            ipAddress = args[1];
        } else {
            ipAddress = "0.0.0.0";
        }
        
        final int port;
        if (args.length >= 3) {
            port = Integer.parseInt(args[2]);
        } else {
            port = 4567;
        }
        
        System.out.println("Using IP address " + ipAddress + 
                           " and port " + port);
        
        Spark.staticFileLocation("/public");
        Spark.ipAddress(ipAddress);
        Spark.port(port);
        
        final CarpoolService carpoolService = new CarpoolService();
        // Insert a dummy TravelRequest
//        TravelRequest req = new TravelRequest(
//                "1", "Iwan", "iwan.littel@technolution.nl", "Delft", 
//                LocalDateTime.now(), LocalDateTime.now().plusHours(1));
//        carpoolService.insertRequest(req);
        // Insert a dummy TravelOffer
//        TravelOffer offer = new TravelOffer(
//                "1", "Iwan", "iwan.littel@technolution.nl", "Delft", 3, 
//                LocalDateTime.now(), Arrays.asList(
//                        new Traveller("Serge", "serge.de.vos@technolution.nl"), 
//                        new Traveller("Jan Jaap", "jan.jaap.treurniet@technolution.nl")
//                ));
//        carpoolService.insertOffer(offer);
        new CarpoolRestEndpoint(carpoolService);
    }
}
