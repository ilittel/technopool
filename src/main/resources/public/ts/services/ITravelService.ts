module technoPool {
    export interface ITravelRequest {
        id: number;
        requesterName: string;
        requesterEmail: string;
        requesterDestination: string;
        desiredLeaveTimeStart: string;
        desiredLeaveTimeEnd: string
    }
    
    export interface ITravelOffer {
        id: number;
        providerName: string;
        providerEmail: string;
        destination: string;
        nrAvailableSeats: number;
        leaveTime: string;
        subscribedTravellers: Array<ITraveller>;
    }
    
    export interface ITraveller {
        name: string;
        email: string
    }
    
    /**
     * Interface for the travel service.
     */
    export interface ITravelService {
        /**
         * Returns an array of travel requests, as a {@link ng.IPromise} instance.
         */
        getTravelRequests() : ng.IPromise<Array<ITravelRequest>>;
        
        /**
         * Returns an array of travel offers, as a {@link ng.IPromise} instance.
         */
        getTravelOffers() : ng.IPromise<Array<ITravelOffer>>;
        
        /**
         * Adds a travel offer.
         */
        addTravelOffer(travelOffer: ITravelOffer) : ng.IPromise<string>;
    }
}
