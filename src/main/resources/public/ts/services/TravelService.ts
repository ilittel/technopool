module technoPool {
    export class TravelService implements ITravelService {
        private httpService: ng.IHttpService;
        /**
         * Prefix that can be used to send web service requests to another host/port.
         */
        private static HOST_PREFIX: string = ""; // http://127.0.0.1:4567";
        
        static $inject = ['$http'];
        constructor($http: ng.IHttpService) {
            this.httpService = $http;
        }
        
        getTravelRequests(): ng.IHttpPromise<Array<ITravelRequest>> {
            return this.httpService.get(TravelService.HOST_PREFIX + "/api/v1/requests");
        }

        getTravelOffers(): ng.IHttpPromise<Array<ITravelOffer>> {
            return this.httpService.get(TravelService.HOST_PREFIX + "/api/v1/offers");
        }
        
        addTravelOffer(travelOffer: ITravelOffer): ng.IHttpPromise<string> {
            return this.httpService.post(TravelService.HOST_PREFIX + "/api/v1/offers", travelOffer);
        }
    }
    
    angular
        .module('technoPool')
        .service('travelService', TravelService);
}
