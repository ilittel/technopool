module technoPool {
    export function poolOffers(): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
              mainCtrl: '=controller'
            },
            templateUrl: "ts/directives/poolOffers.html"
        }
    }
    
    angular
        .module('technoPool')
        .directive('poolOffers', poolOffers);
}
