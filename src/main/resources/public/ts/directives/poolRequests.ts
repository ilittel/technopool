module technoPool {
    export function poolRequests(): ng.IDirective {
        return {
            restrict: 'E',
            scope: {
              mainCtrl: '=controller'
            },
            templateUrl: "ts/directives/poolRequests.html"
        }
    }
    
    angular
        .module('technoPool')
        .directive('poolRequests', poolRequests);
}
