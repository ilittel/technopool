module technoPool {
    'use strict';
    
    export class TravelOfferDialogController {
        static $inject = ['$mdDialog'];
        constructor(private $mdDialog: angular.material.IDialogService, private travelOffer: ITravelOffer) {
        }
        
        hide() : void {
            this.$mdDialog.hide();
        }
        
        cancel() : void {
            this.$mdDialog.cancel();
        }
        
        close() : void {
            this.travelOffer.id = 0;
            this.travelOffer.subscribedTravellers = new Array<ITraveller>();
            this.$mdDialog.hide(this.travelOffer);
        }
    }

    angular
        .module("technoPool")
        .controller('travelOfferDialogController', TravelOfferDialogController);
}
