module technoPool {
    'use strict';

    class TravelOffer implements ITravelOffer {
        id: number;
        providerName: string;
        providerEmail: string;
        destination: string;
        nrAvailableSeats: number;
        leaveTime: string;
        subscribedTravellers: Array<ITraveller>;
    }
    
    /**
     * MainController
     */
    export class MainController {
        private travelRequests: Array<ITravelRequest>;
        private travelOffers: Array<ITravelOffer>;
        
        static $inject = ['travelService', '$mdDialog'];
        constructor(private travelService: ITravelService, private $mdDialog: angular.material.IDialogService) {
            // Fetch the travel requests and offers from the service 
            // during construction of the controller.
            travelService.getTravelRequests().then((result: ng.IHttpPromiseCallbackArg<Array<ITravelRequest>>) => {
               this.travelRequests = result.data;
            });

            travelService.getTravelOffers().then((result: ng.IHttpPromiseCallbackArg<Array<ITravelOffer>>) => {
               this.travelOffers = result.data;
            });
        }
        
        addNewTravelOffer(ev: MouseEvent) : void {
            this.$mdDialog.show({
                  templateUrl: 'traveloffer.tmpl.html',
                  controller: TravelOfferDialogController,
                  controllerAs: 'dlgCtrl',
                  parent: angular.element(document.body),
                  targetEvent: ev,
                  clickOutsideToClose: true,
                  locals: {
                      travelOffer: null
                  },
                  fullscreen: true
            })
                .then((newTravelOffer: ITravelOffer) => {
                    this.storeTravelOffer(newTravelOffer);
                });
        }
        
        storeTravelOffer(newTravelOffer: ITravelOffer) : void {
            this.travelService.addTravelOffer(newTravelOffer)
                // Success handler
                .then((result: ng.IHttpPromiseCallbackArg<string>) => {
                    this.$mdDialog.show(
                        this.$mdDialog.alert()
                            .textContent("Nieuw aanbod is succesvol aangemaakt!")
                            .ok("OK"));
                    this.travelOffers.push(newTravelOffer);
                }),
                // Error handler
                ((result: ng.IHttpPromiseCallbackArg<string>) => {
                    this.$mdDialog.show(
                        this.$mdDialog.alert()
                            .textContent("Nieuw aanbod kon niet worden aangemaakt.<br>Probeer het nog eens.")
                            .ok("OK"));
                });
        }
    }
    
    angular
        .module("technoPool")
        .controller('mainController', MainController);
}
