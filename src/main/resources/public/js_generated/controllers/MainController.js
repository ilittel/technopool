var technoPool;
(function (technoPool) {
    'use strict';
    var TravelOffer = (function () {
        function TravelOffer() {
        }
        return TravelOffer;
    }());
    /**
     * MainController
     */
    var MainController = (function () {
        function MainController(travelService, $mdDialog) {
            var _this = this;
            this.travelService = travelService;
            this.$mdDialog = $mdDialog;
            // Fetch the travel requests and offers from the service 
            // during construction of the controller.
            travelService.getTravelRequests().then(function (result) {
                _this.travelRequests = result.data;
            });
            travelService.getTravelOffers().then(function (result) {
                _this.travelOffers = result.data;
            });
        }
        MainController.prototype.addNewTravelOffer = function (ev) {
            var _this = this;
            this.$mdDialog.show({
                templateUrl: 'traveloffer.tmpl.html',
                controller: technoPool.TravelOfferDialogController,
                controllerAs: 'dlgCtrl',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    travelOffer: null
                },
                fullscreen: true
            })
                .then(function (newTravelOffer) {
                _this.storeTravelOffer(newTravelOffer);
            });
        };
        MainController.prototype.storeTravelOffer = function (newTravelOffer) {
            var _this = this;
            this.travelService.addTravelOffer(newTravelOffer)
                .then(function (result) {
                _this.$mdDialog.show(_this.$mdDialog.alert()
                    .textContent("Nieuw aanbod is succesvol aangemaakt!")
                    .ok("OK"));
                _this.travelOffers.push(newTravelOffer);
            }),
                // Error handler
                (function (result) {
                    _this.$mdDialog.show(_this.$mdDialog.alert()
                        .textContent("Nieuw aanbod kon niet worden aangemaakt.<br>Probeer het nog eens.")
                        .ok("OK"));
                });
        };
        MainController.$inject = ['travelService', '$mdDialog'];
        return MainController;
    }());
    technoPool.MainController = MainController;
    angular
        .module("technoPool")
        .controller('mainController', MainController);
})(technoPool || (technoPool = {}));
