var technoPool;
(function (technoPool) {
    'use strict';
    var TravelOfferDialogController = (function () {
        function TravelOfferDialogController($mdDialog, travelOffer) {
            this.$mdDialog = $mdDialog;
            this.travelOffer = travelOffer;
        }
        TravelOfferDialogController.prototype.hide = function () {
            this.$mdDialog.hide();
        };
        TravelOfferDialogController.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        TravelOfferDialogController.prototype.close = function () {
            this.travelOffer.id = 0;
            this.travelOffer.subscribedTravellers = new Array();
            this.$mdDialog.hide(this.travelOffer);
        };
        TravelOfferDialogController.$inject = ['$mdDialog'];
        return TravelOfferDialogController;
    }());
    technoPool.TravelOfferDialogController = TravelOfferDialogController;
    angular
        .module("technoPool")
        .controller('travelOfferDialogController', TravelOfferDialogController);
})(technoPool || (technoPool = {}));
