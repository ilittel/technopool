var technoPool;
(function (technoPool) {
    var TravelService = (function () {
        function TravelService($http) {
            this.httpService = $http;
        }
        TravelService.prototype.getTravelRequests = function () {
            return this.httpService.get(TravelService.HOST_PREFIX + "/api/v1/requests");
        };
        TravelService.prototype.getTravelOffers = function () {
            return this.httpService.get(TravelService.HOST_PREFIX + "/api/v1/offers");
        };
        TravelService.prototype.addTravelOffer = function (travelOffer) {
            return this.httpService.post(TravelService.HOST_PREFIX + "/api/v1/offers", travelOffer);
        };
        /**
         * Prefix that can be used to send web service requests to another host/port.
         */
        TravelService.HOST_PREFIX = ""; // http://127.0.0.1:4567";
        TravelService.$inject = ['$http'];
        return TravelService;
    }());
    technoPool.TravelService = TravelService;
    angular
        .module('technoPool')
        .service('travelService', TravelService);
})(technoPool || (technoPool = {}));
