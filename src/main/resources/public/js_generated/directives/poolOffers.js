var technoPool;
(function (technoPool) {
    function poolOffers() {
        return {
            restrict: 'E',
            scope: {
                mainCtrl: '=controller'
            },
            templateUrl: "ts/directives/poolOffers.html"
        };
    }
    technoPool.poolOffers = poolOffers;
    angular
        .module('technoPool')
        .directive('poolOffers', poolOffers);
})(technoPool || (technoPool = {}));
