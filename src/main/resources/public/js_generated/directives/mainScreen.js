var technoPool;
(function (technoPool) {
    function mainScreen() {
        return {
            restrict: 'E',
            scope: {
                info: '='
            },
            templateUrl: "ts/directives/mainScreen.html"
        };
    }
    technoPool.mainScreen = mainScreen;
    angular
        .module('technoPool')
        .directive('mainScreen', mainScreen);
})(technoPool || (technoPool = {}));
