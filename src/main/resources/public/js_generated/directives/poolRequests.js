var technoPool;
(function (technoPool) {
    function poolRequests() {
        return {
            restrict: 'E',
            scope: {
                mainCtrl: '=controller'
            },
            templateUrl: "ts/directives/poolRequests.html"
        };
    }
    technoPool.poolRequests = poolRequests;
    angular
        .module('technoPool')
        .directive('poolRequests', poolRequests);
})(technoPool || (technoPool = {}));
