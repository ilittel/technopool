Simple Java-based REST backend for a carpool JavaScript application.

This code runs both locally (using Maven) and in the OpenShift cloud using the 'diy' cartridge. Some modifications were made to the startup hooks in order to get Java 8 and Maven working.

The OpenShift `diy` cartridge documentation can be found at:

http://openshift.github.io/documentation/oo_cartridge_guide.html#diy